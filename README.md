# ledad

ledad - a gemini server

## Build

```
./bootstrap.sh && ./configure && make
```

# Generate a self-signed certificate

```
apt install gnutls-bin
certtool -p --outfile key.pem
certtool --load-privkey key.pem -s --outfile cert.pem
```

# Start

ledad

# Test

```
gnutls-cli --tofu -p 1965 domain.tld
```

```
gemini://domain.tld/
```

Demo page

# Setup content directory

```
mkdir -p .local/share/ledad/servername/
echo -e "# Hello World\nThis is my page" > /home/user/.local/share/ledad/domain.tld/index.gmi
```

