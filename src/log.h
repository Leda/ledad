// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LEDA_LOG_H__
#define _LEDA_LOG_H__

#define LEDA_LOG_LEVEL_NONE    0
#define LEDA_LOG_LEVEL_ERROR   1
#define LEDA_LOG_LEVEL_WARNING 2
#define LEDA_LOG_LEVEL_INFO    3
#define LEDA_LOG_LEVEL_DEBUG   4

typedef enum log {
    NONE = LEDA_LOG_LEVEL_NONE,
    ERROR = LEDA_LOG_LEVEL_ERROR,
    WARN = LEDA_LOG_LEVEL_WARNING,
    INFO = LEDA_LOG_LEVEL_INFO,
    DEBUG = LEDA_LOG_LEVEL_DEBUG
} log_t;

#define LOG_ERROR LEDA_LOG_LEVEL_ERROR
#define LOG_WARN  LEDA_LOG_LEVEL_WARNING
#define LOG_INFO  LEDA_LOG_LEVEL_INFO
#define LOG_DEBUG LEDA_LOG_LEVEL_DEBUG

#include "config.h"

void leda_log_init(int level);
void leda_log(int level, const char* const domain, const char* const msg,
              ...);

#endif // _LEDA_LOG_H__
