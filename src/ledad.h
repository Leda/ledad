// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LEDAD_H__
#define _LEDAD_H__

#include "config.h"

typedef struct _ledad_ctx_t ledad_ctx_t;

ledad_ctx_t* ledad_ctx_init();

void ledad_start(ledad_ctx_t* ctx);

const char* ledad_ctx_get_runtimedir(const ledad_ctx_t* const ctx);

const char* ledad_ctx_get_homedir(const ledad_ctx_t* const ctx);

void ledad_ctx_set_homedir(const char* const dir);

#endif // _LEDAD_H__
