// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ledad.h"
#include "log.h"

#define LOG_DOMAIN "main"

typedef struct
{
    bool version;
    int verbose;
    bool help;
    log_t log;
    char* homedir;
} cmd_options_t;

static bool _cmd_options(cmd_options_t* options, int argc, char* argv[]);
static void _print_version();

int
main(int argc, char* argv[])
{

#ifdef PACKAGE_STATUS_DEVELOPMENT
    printf("!!! *************************** !!!\n");
    printf("!!! Package status: Development !!!\n");
    printf("!!! %-27s !!!\n", PACKAGE_STRING);
    printf("!!! *************************** !!!\n");
#endif

    cmd_options_t options = {
        .version = false, .verbose = 0, .help = false, .log = NONE, .homedir = NULL
    };

    if (!_cmd_options(&options, argc, argv)) {
        _print_version();
        return EXIT_FAILURE;
    }

    printf("Version: %d\n", options.version);
    printf("Verbose: %d\n", options.verbose);
    printf("Help:    %d\n", options.help);
    printf("Log:    %d\n", options.log);
    printf("Homedir:    %s\n", options.homedir);

    ledad_ctx_t* ctx = ledad_ctx_init();
    ledad_start(ctx);

    return EXIT_SUCCESS;
}

static bool
_cmd_options(cmd_options_t* options, int argc, char* argv[])
{
    int c = 0;
    while (1) {
        int option_index = 0;
        static struct option long_options[]
            = { { "help", no_argument, 0, 'h' },
                { "version", no_argument, 0, 'v' },
                { "log", required_argument, 0, 'l' },
                { "verbose", no_argument, 0, 'V' },
                { "homedir", required_argument, 0, 'u' },
                { 0, 0, 0, 0 } };
        c = getopt_long(argc, argv, "hvl:Vu:", long_options, &option_index);
        if (c == -1)
            break;
        switch (c) {
        case 'h':
            options->help = true;
            break;
        case 'v':
            options->version = true;
            break;
        case 'V':
            options->verbose++;
            break;
        case 'l':
            if (strcmp("NONE", optarg) == 0)
                options->log = NONE;
            else if (strcmp("ERROR", optarg) == 0)
                options->log = ERROR;
            else if (strcmp("WARN", optarg) == 0)
                options->log = WARN;
            else if (strcmp("INFO", optarg) == 0)
                options->log = INFO;
            else if (strcmp("DEBUG", optarg) == 0)
                options->log = DEBUG;
            else {
                printf("Unknown log level for -l\n");
                return false;
            }
            break;
        case 'u':
            options->homedir = strdup(optarg);
            break;
        default:
            printf("Wrong parameter - Parameter unknown\n");
        }
    }

    if (optind < argc) {
        return false;
    }

    leda_log_init(options->log);

    leda_log(LOG_INFO, LOG_DOMAIN, "Starting %s ...", PACKAGE_STRING);

    return true;
}

static void
_print_version()
{
    printf("%s\n", PACKAGE_STRING);
}
