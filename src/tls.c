// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */
#include "tls.h"
#include "log.h"
#include <assert.h>
#include <gnutls/gnutls.h>
#include <stdio.h>

#define LOG_DOMAIN "TLS"

#define CHECK(x) assert((x) >= 0)

#define KEYFILE  "key.pem"
#define CERTFILE "cert.pem"
#define CAFILE   "/etc/ssl/certs/ca-certificates.crt"
#define CRLFILE  "crl.pem"

static gnutls_certificate_credentials_t x509_cred;

static void leada_tls_log(int level, const char* m);

void
leada_tls_init()
{
    gnutls_global_set_log_function(leada_tls_log);
    gnutls_global_set_log_level(2);
    CHECK(gnutls_global_init());
    CHECK(gnutls_certificate_allocate_credentials(&x509_cred));
    CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, CAFILE,
                                                 GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_certificate_set_x509_key_file(x509_cred, CERTFILE, KEYFILE,
                                               GNUTLS_X509_FMT_PEM));

#if GNUTLS_VERSION_NUMBER >= 0x030506
    gnutls_certificate_set_known_dh_params(x509_cred, GNUTLS_SEC_PARAM_MEDIUM);
#endif
}

void
leada_tls_session_init(gnutls_session_t* session,
                       gnutls_priority_t* priority_cache)
{
    CHECK(gnutls_init(session, GNUTLS_SERVER));
    CHECK(gnutls_priority_init(priority_cache, NULL, NULL));
    CHECK(gnutls_priority_set(*session, *priority_cache));
    CHECK(gnutls_credentials_set(*session, GNUTLS_CRD_CERTIFICATE, x509_cred));
    gnutls_certificate_server_set_request(*session, GNUTLS_CERT_IGNORE);
    gnutls_handshake_set_timeout(*session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
}

static void
leada_tls_log(int level, const char* m)
{
    leda_log(LOG_INFO, LOG_DOMAIN, "%s", m);
}
