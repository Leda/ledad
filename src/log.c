// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */

#include "log.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define FILENAME "ledad.log"
static int _level = LEDA_LOG_LEVEL_ERROR;
static FILE* _log_file = NULL;

static void _write_log(int level, const char* const domain,
                       const char* const message);

void
leda_log_init(int level)
{
    _level = level;
    _log_file = fopen(FILENAME, "w+");
}

void
leda_log(int level, const char* const domain, const char* const msg, ...)
{
    if (_level >= level) {
        va_list arg;
        char* message = NULL;
        va_start(arg, msg);
        int size_x = vsnprintf(message, 0, msg, arg);
        va_end(arg);
        message = malloc(size_x + 1);
        va_start(arg, msg);
        size_x = vsnprintf(message, size_x + 1, msg, arg);
        va_end(arg);
        _write_log(level, domain, message);
        free(message);
    }
}
static void
_write_log(int level, const char* const domain, const char* const message)
{
    if (_log_file) {
        fprintf(_log_file, "[%s] - %d -  %s\n", domain, level, message);
        fflush(_log_file);
    }
}
