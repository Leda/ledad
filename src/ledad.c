// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */
#include "ledad.h"
#include "assert.h"
#include "log.h"
#include "stdlib.h"

#include <sys/types.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include <strings.h>
#include <sys/stat.h>

#include "tls.h"

#define LOG_DOMAIN "ledad"

#define TCP_IP_PORT    1965
#define LISTEN_BACKLOG 10
#define REQUEST_BUFFER 1024

#define URI_SCHEME_GEMINI "gemini"

#define DEMO_RESPONE \
    "\
20 text/gemini; lang=de \n\
# Project leda daemon \n\
\n\
## Overview \n\
Gemini server in C\n\
\n\
* Small \n\
* Cool \n\
* Fast \n\
"

#define LOOP_CHECK(rval, cmd) \
    do {                      \
        rval = cmd;           \
    } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED)

typedef struct
{
    char* pro;
    int port;
    char* host;
    char* path;
} url_t;

static url_t* _parse_url(const char* const url);

struct _ledad_ctx_t
{
    char* dir_path_runtime;
    char* dir_path_home;
    char* dir_path_logs;
    char* lang;
};

static void _exit_sigint(int s);

static const char* _pidfile(const ledad_ctx_t* ctx);
static const char* _leda_home_dir();

static void _handle_request(ledad_ctx_t* ctx, gnutls_session_t session,
                            char* request);
static char* _load_ressource(ledad_ctx_t* ctx, url_t* url);
static char* _ressource_name(ledad_ctx_t* ctx, url_t* url);

static const char* generate_demo();

ledad_ctx_t*
ledad_ctx_init()
{
    ledad_ctx_t* ctx = malloc(sizeof(ledad_ctx_t));

    ctx->dir_path_runtime = getenv("XDG_RUNTIME_DIR");
    ctx->dir_path_home = (char*)_leda_home_dir();
    ctx->lang = getenv("LANG");

    leda_log(LOG_DEBUG, LOG_DOMAIN, "Homedir path: %s", ctx->dir_path_home);
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Runtime dir path: %s",
             ctx->dir_path_runtime);
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Lang: %s", ctx->lang);

    return ctx;
}

const char*
ledad_ctx_get_runtimedir(const ledad_ctx_t* const ctx)
{
    return ctx->dir_path_runtime;
}

const char*
ledad_ctx_get_homedir(const ledad_ctx_t* const ctx)
{
    return ctx->dir_path_home;
}

void
ledad_start(ledad_ctx_t* ctx)
{
    pid_t pid = getpid();
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Starting %s daemon with pid %d",
             PACKAGE_TARNAME, pid);
    signal(SIGINT, _exit_sigint);
    signal(SIGTERM, _exit_sigint);

    const char* pidfile = _pidfile(ctx);
    FILE* x = fopen(pidfile, "w");
    free((char*)pidfile);
    fprintf(x, "%d", pid);
    fclose(x);
    int fd_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_socket == -1) {
        perror("Socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    /* Clear structure */
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(TCP_IP_PORT);

    if (bind(fd_socket, (struct sockaddr*)&server_addr, sizeof(server_addr))
        == -1) {
        perror("Socket bind error");
        leda_log(LOG_DEBUG, LOG_DOMAIN, "Socket bind error: %s",
                 strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (listen(fd_socket, LISTEN_BACKLOG) == -1) {
        perror("Socket listen error");
        exit(EXIT_FAILURE);
    }

    printf("Listening to port %d. Server ready.\n", TCP_IP_PORT);

    leada_tls_init();

    while (1) {
        gnutls_session_t session;
        gnutls_priority_t priority_cache;
        leada_tls_session_init(&session, &priority_cache);

        struct sockaddr_in cli_addr;
        socklen_t client_len = sizeof(cli_addr);
        int clientfd
            = accept(fd_socket, (struct sockaddr*)&cli_addr, &client_len);
        if (clientfd == -1) {
            perror("accept");
            break;
        }

        gnutls_transport_set_int(session, clientfd);
        int ret;
        LOOP_CHECK(ret, gnutls_handshake(session));
        if (ret < 0) {
            close(clientfd);
            gnutls_deinit(session);
            fprintf(stderr, "TLS - Handshake has failed (%s)\n\n",
                    gnutls_strerror(ret));
            continue;
        }
        printf("TLS - Handshake was completed\n");

        char buffer[REQUEST_BUFFER];
        bzero(buffer, REQUEST_BUFFER);

        char request[REQUEST_BUFFER];
        bzero(request, REQUEST_BUFFER);
        // LOOP_CHECK(ret, gnutls_record_recv(session, buffer, REQUEST_BUFFER));

        do {
            do {
                ret = gnutls_record_recv(session, buffer, REQUEST_BUFFER);
                printf("Read %d ", ret);
            } while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);
            if (ret > 0) {
                strncat(request, buffer, ret);
                printf("Read  %s", request);
            }
        } while (ret > 0 && strchr(buffer, '\n') == NULL);

        if (ret == 0) {
            printf("TLS - Peer has closed the GnuTLS connection\n");
            continue;
        } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
            fprintf(stderr, "TLS - Warning: %s\n", gnutls_strerror(ret));
        } else if (ret < 0) {
            fprintf(stderr, "TLS - Error: %s\n", gnutls_strerror(ret));
            fprintf(
                stderr,
                "TLS -  Received corrupted data(%d). Closing the connection.\n",
                ret);
            continue;
        } else if (ret > 0) {
            _handle_request(ctx, session, request);
            gnutls_bye(session, GNUTLS_SHUT_RDWR);
            close(clientfd);
        }
    }
}

static void
_exit_sigint(int s)
{
    printf("Exit %d\n", s);
    exit(EXIT_SUCCESS);
}

const char*
_pidfile(const ledad_ctx_t* ctx)
{
    size_t size = 0;
    char* filename = NULL;
    /* Determine required size */
    size
        = snprintf(filename, size, "%s/%s", ctx->dir_path_runtime, "ledad.pid");
    filename = malloc(size + 1);
    sprintf(filename, "%s/%s", ctx->dir_path_runtime, "ledad.pid");
    return filename;
}

const char*
_leda_home_dir()
{
    const char* folder = ".local/share/ledad";
    size_t size = 0;

    char* filename = NULL;
    /* Determine required size */
    size = snprintf(filename, size, "%s/%s", getenv("HOME"), folder);
    filename = malloc(size + 1);
    sprintf(filename, "%s/%s", getenv("HOME"), folder);
    return filename;
}

static void
_handle_request(ledad_ctx_t* ctx, gnutls_session_t session, char* request)
{
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Request: %s", request);
    if (strncmp(request, URI_SCHEME_GEMINI, strlen(URI_SCHEME_GEMINI)) == 0) {
        const char* path = ledad_ctx_get_homedir(ctx);
        if (access(path, F_OK) == -1) {
            // No access => show demo
            leda_log(LOG_DEBUG, LOG_DOMAIN, "%s", strerror(errno));
            const char* text = generate_demo();
            gnutls_record_send(session, text, strlen(text));
        } else {
            url_t* url = _parse_url(request);
            if (url) {
                char* ressource = _load_ressource(ctx, url);
                if (ressource) {
                    gnutls_record_send(session, ressource, strlen(ressource));
                } else {
                    char* error = "53 File not found\n";
                    gnutls_record_send(session, error, strlen(error));
                }
            } else {
                char* error = "53 What?\n";
                gnutls_record_send(session, error, strlen(error));
            }
        }
    } else {
        char* error = "53 No gemini scheme\n";
        gnutls_record_send(session, error, strlen(error));
    }
}

static const char*
generate_demo()
{
    return DEMO_RESPONE;
}

static url_t*
_parse_url(const char* const url)
{
    if (!(strstr(url, URI_SCHEME_GEMINI) || strstr(url, "://"))) {
        return NULL;
    }

    url_t* u = malloc(sizeof(url_t));
    bzero(u, sizeof(url_t));

    char* url_tmp = strdup(url);
    if (url_tmp[strlen(url_tmp) - 1] == '\n'
        || url_tmp[strlen(url_tmp) - 1] == '\r') {
        url_tmp[strlen(url_tmp) - 1] = '\0';
    }

    u->pro = strtok(url_tmp, ":/");
    u->host = strdup(strtok(NULL, ":/"));
    u->port = TCP_IP_PORT;
    if (url_tmp) {
        char* p = strtok(NULL, "\r\n");
        if (p)
            u->path = strdup(p);
        free(url_tmp);
    }

    assert(u->pro);
    assert(u->host);
    return u;
}

static char*
_load_ressource(ledad_ctx_t* ctx, url_t* url)
{
    char* filename = _ressource_name(ctx, url);
    if (!filename) {
        return NULL;
    }

    FILE* f = fopen(filename, "r");
    free(filename);

    char* result = malloc(1024 * 10);
    bzero(result, 1024 * 10);
    strcat(result, "20 text/gemini\r\n");
    char buffer[1024 * 10];
    int bs = fread(buffer, 1, (1024 * 10) - 1, f);
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Read '%d'", bs);
    buffer[bs] = '\0';
    strncat(result, buffer, bs);
    fclose(f);
    return result;
}

/**
 * \brief Get filename of the requested resource.
 *
 * This functions builds the filename of the requested resource.
 * Resources should be stored in ctx - dir_path_home.
 *
 * Files are located in <home_path>/hostname/path_to_resource.
 *
 * If the file doesn't exists, return NULL. If the path is a dir,
 * we will add "index.gmi".
 *
 */
static char*
_ressource_name(ledad_ctx_t* ctx, url_t* url)
{
    const char* path = ledad_ctx_get_homedir(ctx);
    char* filename = malloc(1024);
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Hostname '%s'", url->host);
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Path '%s'", url->path);

    if (url->path) {
        sprintf(filename, "%s/%s/%s", path, url->host, url->path);
    } else {
        sprintf(filename, "%s/%s", path, url->host);
    }
    leda_log(LOG_DEBUG, LOG_DOMAIN, "Filename: '%s'", filename);

    if (access(filename, F_OK) == -1) {
        leda_log(LOG_DEBUG, LOG_DOMAIN, "No access to '%s'", filename);
        free(filename);
        return NULL;
    }

    struct stat statbuf;
    if (stat(filename, &statbuf) == -1) {
        leda_log(LOG_DEBUG, LOG_DOMAIN, "No stat for '%s': %s", filename,
                 strerror(errno));
        free(filename);
        return NULL;
    }

    if ((statbuf.st_mode & S_IFMT) == S_IFDIR) {
        leda_log(LOG_DEBUG, LOG_DOMAIN, "File '%s' is a directory", filename);
        char* tmp = strdup(filename);
        sprintf(filename, "%s/%s", tmp, "index.gmi");
        free(tmp);
        if (access(filename, F_OK) == -1) {
            leda_log(LOG_DEBUG, LOG_DOMAIN, "No stat to %s: %s", filename,
                     strerror(errno));
            free(filename);
            return NULL;
        }
        if (stat(filename, &statbuf) == -1) {
            leda_log(LOG_DEBUG, LOG_DOMAIN, "No stat to %s: %s", filename,
                     strerror(errno));
            free(filename);
            return NULL;
        }
    }

    if ((statbuf.st_mode & S_IFMT) != S_IFREG) {
        leda_log(LOG_DEBUG, LOG_DOMAIN, "File %s is not a regfile: %d",
                 filename, statbuf.st_mode);
        free(filename);
        return NULL;
    }

    return filename;
}
