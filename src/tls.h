// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledad.
 *
 * ledad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledad. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LEDAD_TLS_H__
#define _LEDAD_TLS_H__

#include <gnutls/gnutls.h>

void leada_tls_init();

void leada_tls_session_init(gnutls_session_t* session,
                            gnutls_priority_t* priority_cache);

#endif // _LEDAD_TLS_H__
